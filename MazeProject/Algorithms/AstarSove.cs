﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Algorithms
{
   public class AstarSolve
    {
        GridWatcher grd;

        public AstarSolve(GridWatcher _grid)
        {
            this.grd = _grid;

            foreach (var x in _grid.Nodes)
            {               
                x.ManhattanDistanceToTarget = CalculateManhattan(x); 
                x.Visited = false;
                x.MinCostToStart = 0;
                x.ParentNode = null;
            }
        }
        public List<GridNode> TrySolving()
        {
            var list = Solve();
            return list;
        }

        List<GridNode> GeneratePath()
        {
            GridNode temp = grd.EndNode;
            List<GridNode> path = new List<GridNode>();
            path.Add(temp);
            while (temp.ParentNode != null)
            {
                path.Add(temp.ParentNode);
                temp = temp.ParentNode;
            }
            path.Reverse();
            return path;
        }


        public List<GridNode> Solve()
        {
            GridNode StartNode = grd.StartNode;
            StartNode.MinCostToStart = 0;

            var prioQueue = new List<GridNode>();
            prioQueue.Add(StartNode);
            do
            {
                prioQueue = prioQueue.OrderBy(x => x.MinCostToStart + x.ManhattanDistanceToTarget).ToList();
                var node = prioQueue.First();
                prioQueue.Remove(node);
                foreach (var childNode in node.Neighbors.OrderBy(x => x.Cost ))
                {
                    if (childNode.Visited)
                        continue;
                    if (childNode.MinCostToStart == 0 ||
                        node.MinCostToStart + childNode.Cost < childNode.MinCostToStart)
                    {
                        childNode.MinCostToStart = node.MinCostToStart + childNode.Cost;
                        childNode.ParentNode = node;
                        if (!prioQueue.Contains(childNode))
                            prioQueue.Add(childNode);
                    }
                }
                node.Visited = true;
                if (node.NodeLocation.Row == grd.EndNode.NodeLocation.Row && node.NodeLocation.Column == grd.EndNode.NodeLocation.Column)
                    return GeneratePath();
            } while (prioQueue.Any());

            return null;
        }

        public int CalculateManhattan(GridNode Node)
        {
           return Math.Abs(Node.NodeLocation.Column - grd.EndNode.NodeLocation.Column) + Math.Abs(Node.NodeLocation.Row - grd.EndNode.NodeLocation.Row);
        }

    }
}
