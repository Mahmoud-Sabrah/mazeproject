﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Algorithms
{
    class MiniMax
    {
        GridWatcher grd;

        public MiniMax(GridWatcher _grid)
        {
            this.grd = _grid;

            foreach (var x in _grid.Nodes)
            {
                x.Visited = false;
                x.MinCostToStart = 0;
                x.ParentNode = null;
            }
        }
        public List<GridNode> Solve()
        {
            GridNode StartNode = grd.StartNode;
            StartNode.MinCostToStart = 0;

            var q = new List<GridNode>();
            q.Add(StartNode);
            do
            {
                q = q.OrderBy(x => x.MinCostToStart).ToList();
                var node = q.First();
                q.Remove(node);
                foreach (var childNode in node.Neighbors.OrderBy(x => x.Cost))
                {
                    if (childNode.Visited)
                        continue;
                    if (childNode.MinCostToStart == 0 ||
                        node.MinCostToStart + childNode.Cost < childNode.MinCostToStart)
                    {
                        childNode.MinCostToStart = node.MinCostToStart + childNode.Cost;
                        childNode.ParentNode = node;
                        if (!q.Contains(childNode))
                            q.Add(childNode);
                    }
                }
                node.Visited = true;
                if (node.NodeLocation.Row == grd.EndNode.NodeLocation.Row && node.NodeLocation.Column == grd.EndNode.NodeLocation.Column)
                    return GeneratePath();
            } while (q.Any());

            return null;
        }

        List<GridNode> GeneratePath()
        {
            GridNode temp = grd.EndNode;
            List<GridNode> path = new List<GridNode>();
            path.Add(temp);
            while (temp.ParentNode != null)
            {
                path.Add(temp.ParentNode);
                temp = temp.ParentNode;
            }
            path.Reverse();
            return path;
        }



    }
}
