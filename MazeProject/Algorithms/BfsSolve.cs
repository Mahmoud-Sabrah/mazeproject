﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MazeProject.Algorithms
{
    public class BfsSolve
    {
        GridWatcher grd;
        Queue<GridNode> BfsQueue;
        List<GridNode> Path;

        public BfsSolve(GridWatcher _grid)
        {
            this.grd = _grid;
            Path = new List<GridNode>();
            BfsQueue = new Queue<GridNode>();
        }

    
       public List<GridNode> Solve()
         {
            BfsQueue.Enqueue(grd.StartNode);

            while (BfsQueue.Count > 0)
            {
                var element = BfsQueue.Dequeue();

                if (!element.Visited)
                    element.Visited = true;

                    foreach (var item in element.Neighbors)
                    {
                        if (item.Visited)
                            continue;

                        BfsQueue.Enqueue(item);
                        item.ParentNode = element;
                        if (item.NodeLocation.Row == this.grd.EndNode.NodeLocation.Row && item.NodeLocation.Column == this.grd.EndNode.NodeLocation.Column)
                            return GeneratePath();                        
                    }
                
            }

            return null;
        }


        List<GridNode> GeneratePath()
        {
            GridNode temp = grd.EndNode;
            List<GridNode> path = new List<GridNode>();
            path.Add(temp);
            while (temp.ParentNode != null)
            {
                path.Add(temp.ParentNode);
                temp = temp.ParentNode;
            }
            path.Reverse();
            return new List<GridNode>(path);
        }
    }
}
