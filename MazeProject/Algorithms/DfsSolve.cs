﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MazeProject.Algorithms
{
    public class DfsSolve
    {
        GridWatcher grd;
        Stack<GridNode> Dfstack;

    
        public DfsSolve(GridWatcher _grid)
        {
            this.grd = _grid;
            Dfstack = new Stack<GridNode>();
        }

        public List<GridNode> Solve()
        {
            Dfstack.Push(grd.StartNode);


            while (Dfstack.Count() != 0)
            {
                GridNode node = Dfstack.Pop();

                if (node.Visited != true)
                    node.Visited = true;
                
                foreach (GridNode x in node.Neighbors)
                {
                    if (x.Visited)
                        continue;

                    x.ParentNode = node;
                    if (x.NodeLocation.Row == this.grd.EndNode.NodeLocation.Row && x.NodeLocation.Column == this.grd.EndNode.NodeLocation.Column)                    
                        return GeneratePath();                   
                    Dfstack.Push(x);

                }
            }
            return null;
        }

         List<GridNode> GeneratePath()
         {
            GridNode temp = grd.EndNode;
            List<GridNode> path = new List<GridNode>();
            path.Add(temp);
            while (temp.ParentNode != null)
            {
                path.Add(temp.ParentNode);
                temp = temp.ParentNode;
            }
            path.Reverse();
            return new List<GridNode>(path);
         }
    }

}
