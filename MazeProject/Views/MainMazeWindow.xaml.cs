﻿using MazeProject.Algorithms;
using MazeProject.Enums;
using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MazeProject.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainMazeWindow : Window
    {
        public GridWatcher _base { get; set; }


        public MainMazeWindow()
        {
            InitializeComponent();

   
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            grd.OnCellClicked += Grd_OnCellClicked;

        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        
        private void Btn_create_Click(object sender, RoutedEventArgs e)
        {
            _base = CreateMaze();
            grd.ReloadGrid(  _base._maze,_base.StartNode.NodeLocation,_base.EndNode.NodeLocation, _base.PlayerNode.NodeLocation,_base.CurrentPath);
        }

        public GridWatcher CreateMaze()
        {
            GridWatcher temp = new GridWatcher(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text),
              new GridLocation { Row = int.Parse( txt_startrow.Text), Column = int.Parse(txt_startColumn.Text) },
              new GridLocation { Row = int.Parse(txt_endrow.Text), Column = int.Parse(txt_endColumn.Text) }
             );

            txt_rows.Text = temp.GridRows.ToString();
            txt_columns.Text = temp.GridColumns.ToString();
            txt_startColumn.Text = temp.StartNode.NodeLocation.Column.ToString();
            txt_startrow.Text = temp.StartNode.NodeLocation.Row.ToString();
            txt_endColumn.Text = temp.EndNode.NodeLocation.Column.ToString();
            txt_endrow.Text = temp.EndNode.NodeLocation.Row.ToString();
            lbl_cost.Content = 0;
            lbl_states.Content = "State:1";
            lbl_realtimeresult.Content = "Moving Response";
            btn_reload.IsEnabled = true;
            btn_showstates.IsEnabled = true;
            return temp;
        }


        public void Reload()
        {
            _base.ReloadWithTheSameMaze();
            grd.ReloadGrid(_base._maze, _base.StartNode.NodeLocation, _base.EndNode.NodeLocation, _base.PlayerNode.NodeLocation, _base.CurrentPath);
            ReloadUI();
            lbl_states.Content = "State:1";
        }

        public void ReloadUI()
        {
            txt_rows.Text = _base.GridRows.ToString();
            txt_columns.Text = _base.GridColumns.ToString();
            txt_startColumn.Text = _base.StartNode.NodeLocation.Column.ToString();
            txt_startrow.Text = _base.StartNode.NodeLocation.Row.ToString();
            txt_endColumn.Text = _base.EndNode.NodeLocation.Column.ToString();
            txt_endrow.Text = _base.EndNode.NodeLocation.Row.ToString();
            lbl_cost.Content = "Cost:0";
            lbl_realtimeresult.Content = "Moving Response";
            lbl_states.Content = "State:1";
            btn_reload.IsEnabled = true;
            btn_showstates.IsEnabled = true;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (_base == null || (e.Key!=Key.Right && e.Key != Key.Left && e.Key != Key.Up && e.Key != Key.Down))
                return;

            if (_base.PlayerNode.NodeLocation.Row == _base.EndNode.NodeLocation.Row && _base.PlayerNode.NodeLocation.Column == _base.EndNode.NodeLocation.Column)
            {
                if (MessageBox.Show("Can not move because the player location is the same as final location" + Environment.NewLine + "Do you want to reload the game?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    Reload();
                return;
            }


            ResultType res;
            Direction dir;
            GridLocation Destination = new GridLocation();
            if (e.Key == Key.Right)
            {
                Destination = (new GridLocation { Row = _base.PlayerNode.NodeLocation.Row, Column = _base.PlayerNode.NodeLocation.Column + 1 });
                dir = Direction.Right;
            }
            else if (e.Key == Key.Left)
            {
                Destination = (new GridLocation { Row = _base.PlayerNode.NodeLocation.Row, Column = _base.PlayerNode.NodeLocation.Column - 1 });
                dir = Direction.Left;
            }

            else if (e.Key == Key.Up)
            {
                Destination = (new GridLocation { Row = _base.PlayerNode.NodeLocation.Row - 1, Column = _base.PlayerNode.NodeLocation.Column });
                dir = Direction.Up;
            }

            else if (e.Key == Key.Down)
            {
                Destination = (new GridLocation { Row = _base.PlayerNode.NodeLocation.Row + 1, Column = _base.PlayerNode.NodeLocation.Column });
                dir = Direction.Down;
            }

            else
                return;

            MoveByUI(Destination, dir);

        }

        private void Grd_OnCellClicked(GridLocation Destination)
        {
            if (_base == null)
                return;
            if (_base.PlayerNode.NodeLocation.Row == _base.EndNode.NodeLocation.Row && _base.PlayerNode.NodeLocation.Column == _base.EndNode.NodeLocation.Column)
            {
                if (MessageBox.Show("Can not move because the player location is the same as final location" + Environment.NewLine + "Do you want to reload the game?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    Reload();
                return;
            }
            //MoveByUI(Destination);
        }

        public void MoveByUI(GridLocation Destination,Direction To)
        {
            ResultType res;
            res = _base.Move(To);
            Console.WriteLine(res.ToString());
            if (res == ResultType.Done || res == ResultType.EndState || res == ResultType.BeginState)
                grd.Move(Destination);


            lbl_cost.Content = "Cost:" + _base.TotalCost;
            lbl_realtimeresult.Content = res.ToString();
            lbl_states.Content = "States:" + _base.CurrentPath.Count();
        }

        private void Btn_reload_Click(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        private void Rb_manually_Checked(object sender, RoutedEventArgs e)
        {
            MazeManualCells aa = new MazeManualCells(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text));
            if (aa.ShowDialog() == false)
            {
                rb_random.IsChecked = true;
                return;
            }
            else
            {
                GridWatcher temp = new GridWatcher(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text),
                  new GridLocation { Row = int.Parse(txt_startrow.Text), Column = int.Parse(txt_startColumn.Text) },
                  new GridLocation { Row = int.Parse(txt_endrow.Text), Column = int.Parse(txt_endColumn.Text) }, aa.Cells);

                grd.ReloadGrid(temp._maze, temp.StartNode.NodeLocation, temp.EndNode.NodeLocation, temp.PlayerNode.NodeLocation, temp.CurrentPath);
                _base = temp;
                ReloadUI();
            }
        }

        private void CommonCommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Btn_showstates_Click(object sender, RoutedEventArgs e)
        {
            MazeViewStates viewer = new MazeViewStates(_base);
            viewer.Show();
        }

        private void Btn_undo_Click(object sender, RoutedEventArgs e)
        {
            _base.UndoLastState();
            lbl_cost.Content = "Cost:" + _base.TotalCost;
            //MessageBox.Show(_base.CurrentPath.Last().Row.ToString() + "," + _base.CurrentPath.Last().Column.ToString());
            //MessageBox.Show(_base.CurrentPlayerLocation.Row.ToString() + "," + _base.CurrentPlayerLocation.Column.ToString());
            //MessageBox.Show(_base.StatesCount.ToString());
            lbl_states.Content = "States:" + _base.TotalCost;
        }

        private void Btn_dfs_Click(object sender, RoutedEventArgs e)
        {

            GridWatcher temp = new GridWatcher(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text),
            new GridLocation { Row = int.Parse(txt_startrow.Text), Column = int.Parse(txt_startColumn.Text) },
            new GridLocation { Row = int.Parse(txt_endrow.Text), Column = int.Parse(txt_endColumn.Text) },_base._maze
           );

            DfsSolve dfs = new DfsSolve(temp);
            var path = dfs.Solve();
            if(path == null)
            {
                MessageBox.Show("Can not solve it");
                return ;
            }
            temp.CurrentPath = path;
            MazeViewStates viewer = new MazeViewStates(temp);
            viewer.Title = "Dfs Solve";
            viewer.Show();
        }

        private void Btn_bfs_Click(object sender, RoutedEventArgs e)
        {
            GridWatcher temp = new GridWatcher(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text),
            new GridLocation { Row = int.Parse(txt_startrow.Text), Column = int.Parse(txt_startColumn.Text) },
            new GridLocation { Row = int.Parse(txt_endrow.Text), Column = int.Parse(txt_endColumn.Text) }, _base._maze
           );

            BfsSolve bfs = new BfsSolve(temp);
            var path = bfs.Solve();
            if (path == null)
            {
                MessageBox.Show("Can not solve it");
                return;
            }
            temp.CurrentPath = path;
            MazeViewStates viewer = new MazeViewStates(temp);
            viewer.Title = "Bfs Solve";
            viewer.Show();
        }

        private void Btn_dijkstra_Click(object sender, RoutedEventArgs e)
        {
            GridWatcher temp = new GridWatcher(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text),
            new GridLocation { Row = int.Parse(txt_startrow.Text), Column = int.Parse(txt_startColumn.Text) },
            new GridLocation { Row = int.Parse(txt_endrow.Text), Column = int.Parse(txt_endColumn.Text) }, _base._maze
           );

            DijkstraSolve dijkstra = new DijkstraSolve(temp);
            var path = dijkstra.Solve();
            if (path == null)
            {
                MessageBox.Show("Can not solve it");
                return;
            }
            temp.CurrentPath = path;
            MazeViewStates viewer = new MazeViewStates(temp);
            viewer.Title = "Dijkstra Solve";
            viewer.Show();
        }

        private void Btn_astar_Click(object sender, RoutedEventArgs e)
        {
            GridWatcher temp = new GridWatcher(int.Parse(txt_rows.Text), int.Parse(txt_columns.Text),
            new GridLocation { Row = int.Parse(txt_startrow.Text), Column = int.Parse(txt_startColumn.Text) },
            new GridLocation { Row = int.Parse(txt_endrow.Text), Column = int.Parse(txt_endColumn.Text) }, _base._maze
            );

            AstarSolve astar = new AstarSolve(temp);
            var path = astar.TrySolving();
            if (path == null)
            {
                MessageBox.Show("Can not solve it");
                return;
            }
            temp.CurrentPath = path;
            MazeViewStates viewer = new MazeViewStates(temp);
            viewer.Title = "Astar Solve";
            viewer.Show();
        }
    }
}
