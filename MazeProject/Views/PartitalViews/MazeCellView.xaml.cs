﻿using MazeProject.Enums;
using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MazeProject.Views.PartitalViews
{
    /// <summary>
    /// Interaction logic for MazeCellView.xaml
    /// </summary>
    public partial class MazeCellView : UserControl
    {
        public GridCellType CellType { get; set; }
        public bool IsItBeginCell { get; set; } = false;
        public bool IsItEndCell { get; set; } = false;
        public bool IsItPlayer { get; set; } = false;
        public GridLocation CellLocation { get; set; }
        public bool WithFullCost { get; set; } = false;

        public MazeCellView()
        {
            InitializeComponent();
        }
        public MazeCellView(GridCellType CellType,bool IsItBeginCell,bool IsItEndCell) :this()
        {
            this.CellType = CellType;
            this.IsItBeginCell = IsItBeginCell;
            this.IsItEndCell = IsItEndCell;
            this.IsItPlayer = IsItBeginCell;
        }

        private void Grd_Loaded(object sender, RoutedEventArgs e)
        {
            if (CellType == Enums.GridCellType.Path)
            {
                grd.Background = Brushes.Gray;
                txt_title.Content = "Path";
            }
            else if (CellType == Enums.GridCellType.Mountain)
            {
                grd.Background = Brushes.SaddleBrown;
                txt_title.Content = "Mountain";
            }
            else if (CellType == Enums.GridCellType.Sand)
            {
                grd.Background = Brushes.SandyBrown;
                txt_title.Content = "Sand";
            }
            else if (CellType == Enums.GridCellType.Water)
            {
                grd.Background = Brushes.LightSkyBlue;
                txt_title.Content = "Water";
            }


            if (IsItBeginCell)
            {
                txt_title.Content += ",B";
                brd_ofplayer.BorderBrush = Brushes.Yellow;
            }
                
            if (IsItEndCell)
            {
                txt_title.Content += ",E";
                brd_ofplayer.BorderBrush = Brushes.Green;
            }
             


            if(IsItPlayer)
                SetAsPlayer();

        }

        public void SetAsPlayer()
        {
            IsItPlayer = true;
            string val = txt_title.Content.ToString() + ",P";
            txt_title.Content = val;

            if(!IsItBeginCell && !IsItEndCell)
                brd_ofplayer.BorderBrush = Brushes.Blue;
        }

        public void SetAsDefault()
        {
            IsItPlayer = false;
            txt_title.Content = txt_title.Content.ToString().Substring(0, txt_title.Content.ToString().Length - 2);
            if (!IsItBeginCell && !IsItEndCell)
                brd_ofplayer.BorderBrush = Brushes.Transparent;
        }

        public void SetAsNextMove(bool SetIt)
        {
            if (SetIt)
            {
                grd.Background = Brushes.Aquamarine;
            }

            else
                grd.Background = Brushes.Transparent;
        }


        

}
}
