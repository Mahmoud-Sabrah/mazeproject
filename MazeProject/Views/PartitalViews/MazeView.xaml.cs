﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MazeProject.Views.PartitalViews
{
    /// <summary>
    /// Interaction logic for MazeView.xaml
    /// </summary>
    public partial class MazeView : UserControl
    {
        public GridCell[,] _maze { get; set; }
        public GridLocation StartLocation { get; set; }
        public GridLocation EndLocation { get; set; }
        public GridLocation CurrentPlayerLocation { get; set; }
        public List<GridNode> CurrentPath { get; set; }
        MazeCellView PlayerCell;

        public delegate void MoveByGrid(GridLocation CellLocation);
        public event MoveByGrid OnCellClicked;



        public MazeView()
        {
            InitializeComponent();
            CurrentPath = new List<GridNode>();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }


        public void ReloadGrid(GridCell[,] Cells,GridLocation StartLocation,GridLocation EndLocation,GridLocation CurrentPlayerLocation, List<GridNode> CurrentPath )
        {
            this._maze = Cells;

            this.StartLocation = StartLocation;
            this.EndLocation = EndLocation;
            this.CurrentPlayerLocation = CurrentPlayerLocation;
            this.CurrentPath = CurrentPath;

            grd_maze.Children.Clear();
            grd_maze.ColumnDefinitions.Clear();
            grd_maze.RowDefinitions.Clear();

            for (int i = 0; i < this._maze.GetLength(0); i++)
                grd_maze.RowDefinitions.Add(new RowDefinition());

            for (int j = 0; j < this._maze.GetLength(1); j++)
                grd_maze.ColumnDefinitions.Add(new ColumnDefinition());

            for(int i=0; i<this._maze.GetLength(0);i++)
            {
                for (int j = 0; j < this._maze.GetLength(1); j++)
                {
                    MazeCellView btnn = new MazeCellView();
                    btnn.CellType = Cells[i, j].CellType;
                    if (i == StartLocation.Row - 1 && j == StartLocation.Column - 1)
                        btnn.IsItBeginCell = true;
                    if(i == EndLocation.Row - 1 && j == EndLocation.Column - 1)
                    {
                        btnn.IsItEndCell = true;
                    }                    
                   if(i == CurrentPlayerLocation.Row - 1 && j == CurrentPlayerLocation.Column - 1)
                    {
                        //btnn.IsItPlayerCell = true;
                        btnn.IsItPlayer = true;
                        PlayerCell = btnn;
                    }

                    btnn.CellLocation = new GridLocation { Row = i+1, Column = j+1};

                    Grid.SetRow(btnn, i);
                    Grid.SetColumn(btnn, j);
                    grd_maze.Children.Add(btnn);

                    btnn.MouseUp += Btnn_MouseUp;
                }
            }

        }



        public void Move(GridLocation To)
        {
            //RemoveNextMoves();
            PlayerCell.SetAsDefault();
            PlayerCell = (MazeCellView)grd_maze.Children.Cast<UIElement>().First(x => Grid.GetRow(x) == To.Row-1 && Grid.GetColumn(x) == To.Column-1);
            //SetNextMoves(AvailableNextMoves);
            PlayerCell.SetAsPlayer();
           
            //Grid.SetRow(, To.Row - 1);
            //grd_maze.Children.

        }

        private void Btnn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(OnCellClicked !=null)
            {
                OnCellClicked(((MazeCellView)sender).CellLocation);
            }          
        }



        void RemoveNextMoves()
        {
            foreach(GridNode To in CurrentPath)
            {
              ((MazeCellView)grd_maze.Children.Cast<UIElement>().First(x => Grid.GetRow(x) == To.NodeLocation.Row - 1 && Grid.GetColumn(x) == To.NodeLocation.Column - 1)).SetAsNextMove(false);
            }
        }

        void SetNextMoves(List<GridLocation> AvailableNextMoves)
        {
            foreach (GridLocation To in AvailableNextMoves)
            {
                ((MazeCellView)grd_maze.Children.Cast<UIElement>().First(x => Grid.GetRow(x) == To.Row - 1 && Grid.GetColumn(x) == To.Column - 1)).SetAsNextMove(true);
            }
        }
        

}
}
