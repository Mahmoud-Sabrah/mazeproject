﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MazeProject.Views
{
    /// <summary>
    /// Interaction logic for MazeManualCells.xaml
    /// </summary>
    public partial class MazeManualCells : Window
    {

        public  GridCell[,] Cells;
        int CurrentRow = 0;
        int CurrentColumn = 0;
        bool filled = false;
        GridLocation[] temp;

        public MazeManualCells(int rows,int columns)
        {
            InitializeComponent();
            Cells = new GridCell[rows, columns];

            //for (int i = 0; i <Cells.GetLength(0);i++)
            //{
            //    for (int j = 0; j < Cells.GetLength(1); j++)
            //    {
            //        temp[i,j] = new GridLocation { Row = i, Column = j };
            //    }
            //}

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

  
        private void Btn_addcell_Click(object sender, RoutedEventArgs e)
        {
            Cells[CurrentRow, CurrentColumn] = new GridCell((Enums.GridCellType)cmb_types.SelectedIndex);
            CurrentColumn++;
            if (CurrentColumn == Cells.GetLength(1))
                if (CurrentRow + 1 != Cells.GetLength(0))
                {
                    CurrentRow++;
                    CurrentColumn = 0;
                }
                else
                {
                    Window.GetWindow(this).DialogResult = true;
                    Window.GetWindow(this).Close();
                }

            lbl_.Content = string.Format("({0},{1})", CurrentRow, CurrentColumn);
        }
    }
}
