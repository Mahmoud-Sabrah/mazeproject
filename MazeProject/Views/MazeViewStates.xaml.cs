﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MazeProject.Views
{
    /// <summary>
    /// Interaction logic for MazeViewStates.xaml
    /// </summary>
    public partial class MazeViewStates : Window
    {
        GridWatcher _watcher;
        


        public MazeViewStates()
        {
            InitializeComponent();
        }
       public MazeViewStates(GridWatcher _mazewatcher) :this()
        {
            this._watcher = _mazewatcher;
            AddStates();
        }


       public void AddStates()
        {
            int i = 0;
            foreach (GridNode x in _watcher.CurrentPath)
            {
                ListViewItem y = new ListViewItem();
                y.Content = (++i).ToString();
                y.MouseDoubleClick += Y_MouseDoubleClick;
                lst_items.Items.Add(y);
            }
        }

        private void Y_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int stateNumber = int.Parse(((ListViewItem)sender).Content.ToString()); 
            GridNode x = _watcher.CurrentPath.Skip(stateNumber - 1).First();
            grd.ReloadGrid(_watcher._maze, _watcher.StartNode.NodeLocation, _watcher.EndNode.NodeLocation, x.NodeLocation, _watcher.CurrentPath);
            if(x != _watcher.CurrentPath.First())
            {
                lbl_cost.Content = "Cost:" + (_watcher.CurrentPath.TakeWhile(y => y != x).Skip(1).Sum(y => y.Cost) + x.Cost);
            }
            else
            {
                lbl_cost.Content = "Cost:0";
            }

        }

        private void Lst_items_KeyUp(object sender, KeyEventArgs e)
        {
            Y_MouseDoubleClick((ListBoxItem)lst_items.SelectedItem, null);
        }
    }
}
