﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Enums
{
    public enum ResultType
    {
        BeginState,
        EndState,
        OutOfGrid,
        CanNotMoveBecauseOfWater,
        CanNotMoveBecauseItIllegalMove,
        SameAsLastMove,
        Done
    }

    public static class ResultHanderType
    {
        public static ResultType[] FailResults = new ResultType[]
        { ResultType.CanNotMoveBecauseOfWater, ResultType.CanNotMoveBecauseItIllegalMove, ResultType.OutOfGrid, ResultType.SameAsLastMove };
    }
}
