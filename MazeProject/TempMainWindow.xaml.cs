﻿using MazeProject.Enums;
using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MazeProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class TempMainWindow : Window
    {
        MazeWatcher _base;

        public TempMainWindow()
        {
            InitializeComponent();


            //MazeCell _maze = new MazeCell(Enums.CellType.Mountain);
            //lbl1.Content = _maze.CellType.ToString();
            //lbl1.Content = _maze.Cost.ToString();

            
        }

        public void CreateMaze()
        {
            
             _base = new MazeWatcher(4,4,
                new GridLocation { Row = 1 ,Column=1},
                new GridLocation { Row = 4, Column = 4 }
               );


            for (int i = 0; i < _base._maze.GetLength(0); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < _base._maze.GetLength(1); j++)
                    Console.Write(_base._maze[i, j].CellType + " ");
            }

            Console.WriteLine();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Txt_output_KeyUp(object sender, KeyEventArgs e)
        {

        }



        void Move(int Row,int Column)
        {
            txt_output.Text = "";

            txt_result.Text = _base.Move(new GridLocation() { Row =Row, Column = Column }).ToString();
            for (int i = 0; i < _base._maze.GetLength(0); i++)
            {
                for (int j = 0; j < _base._maze.GetLength(1); j++)
                {
                    txt_output.Text += _base._maze[i, j].CellType.ToString();

                    if (i == _base.StartLocation.Row - 1 && j == _base.StartLocation.Column - 1)
                        txt_output.Text += "B";
                    if (i == _base.EndLocation.Row - 1 && j == _base.EndLocation.Column - 1)
                        txt_output.Text += "E";
                    if (i == _base.CurrentPlayerLocation.Row - 1 && j == _base.CurrentPlayerLocation.Column - 1)
                        txt_output.Text += "P";
                    txt_output.Text += " ";
                }
                txt_output.Text += Environment.NewLine;
                txt_totalcost.Text = _base.CurrentCost.ToString();
            }
        }

        private void Grid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
                Move(_base.CurrentPlayerLocation.Row, _base.CurrentPlayerLocation.Column + 1);
            else if (e.Key == Key.Left)
                Move(_base.CurrentPlayerLocation.Row, _base.CurrentPlayerLocation.Column - 1);
            else if (e.Key == Key.Up)
                Move(_base.CurrentPlayerLocation.Row - 1, _base.CurrentPlayerLocation.Column);
            else if (e.Key == Key.Down)
                Move(_base.CurrentPlayerLocation.Row + 1, _base.CurrentPlayerLocation.Column);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CreateMaze();
            Move(int.Parse(txt_row.Text), int.Parse(txt_column.Text));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).DialogResult = true;
            Window.GetWindow(this).Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Window.GetWindow(this).DialogResult = false ;
        }
    }
}
