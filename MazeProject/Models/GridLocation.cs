﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Models
{
   public struct GridLocation
    {
        public int Row { get; set; }
        public int Column { get; set; }
    }
}
