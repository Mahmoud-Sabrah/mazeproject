﻿using MazeProject.Enums;
using MazeProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MazeProject.Models
{
    public class MazeState : IMazeSharedMethods
    {
        public MazeWatcher Settings;
        public GridCell[,] GridArr { get; private set; }
        public GridLocation CurrentStateLocation;
        public List<GridLocation> CurrentPath;
        public List<GridLocation> NextMoves;
        public List<MazeState> NextStates;
        public int CurrentCost { get; private set; }
        public int CurrentStateNumber { get; set; } = 0;


        public MazeState(MazeWatcher _settings)
        {
            this.Settings = _settings;
            this.GridArr = _settings._maze; 
            this.CurrentStateLocation = _settings.CurrentPlayerLocation;
            this.CurrentPath =new List<GridLocation>(_settings.CurrentPath);
            this.CurrentCost = _settings.CurrentCost;
            this.CurrentStateNumber = (++_settings.StatesCount);
            this.NextMoves = this.AvailableNextMoves();
            //this.NextStates = this.AvailableNextStates();
        }

        //public MazeState(MazeWatcher _settings,int Cost, GridLocation CurrentStateLocation, List<GridLocation> CurrentStatePath)
        //{
        //    this.Settings = _settings;
        //    this.GridArr = _settings._maze;
        //    this.CurrentStateLocation = CurrentStateLocation;
        //    this.CurrentPath = new List<GridLocation>(CurrentStatePath);
        //    this.CurrentCost = Cost;
        //    this.CurrentStateNumber = _settings.StatesCount+1;
        //    this.NextMoves = this.AvailableNextMoves();
        //    this.NextStates = this.AvailableNextStates();
        //}
        public ResultType Move(GridLocation Destination)
        {

            var fff = Destination;
            var _res= CheckValidateOfTheDestination(Destination);


            if (ResultHanderType.FailResults.Any(x=>x == _res) )
                return _res;


            Settings.CurrentPath.Add(Destination); //store the destination state location int the current state 
            Settings.CurrentCost += GridArr[Destination.Row - 1, Destination.Column - 1].Cost;
            Settings.CurrentPlayerLocation = new GridLocation { Row = Destination.Row, Column = Destination.Column };
            Settings.States.Add(new MazeState(Settings));         
            return ResultType.Done;
        }         
        public ResultType CheckValidateOfTheDestination(GridLocation Destination)
        {
            if (IsItOutOfGrid(Destination))
                return ResultType.OutOfGrid;
            else if (IsItIllegalMove(Destination))
                return ResultType.CanNotMoveBecauseItIllegalMove;
            else if (IsItWater(Destination))
                return ResultType.CanNotMoveBecauseOfWater;
            else if (IsItBeginState(Destination))
                return ResultType.BeginState;
            else if (IsItEndState(Destination))
                return ResultType.EndState;
            else if (Destination.Row == CurrentStateLocation.Row && Destination.Column == CurrentStateLocation.Column)
                return ResultType.SameAsLastMove;
            return ResultType.Done;
        }        
        public bool IsItOutOfGrid(GridLocation Destination)
        {
            if (Destination.Row > Settings.GridRows || 
                Destination.Column > Settings.GridColumns ||
                Destination.Row <= 0 || // <= 0 because the user must start from (1) as index not zero 
                Destination.Column <= 0 
                )
                return true;

            return false;

        }
        public bool IsItWater(GridLocation Destination)
        {
            if (GridArr[Destination.Row - 1, Destination.Column - 1].CellType == GridCellType.Water)
                return true;
            return false;
        }
        public bool IsItIllegalMove(GridLocation Destination)
        {
            if (Destination.Row < CurrentStateLocation.Row - 1 ||
                            Destination.Column < CurrentStateLocation.Column - 1 ||
                            (Destination.Row == CurrentStateLocation.Row - 1 && Destination.Column == CurrentStateLocation.Column + 1) ||
                            (Destination.Row == CurrentStateLocation.Row - 1 && Destination.Column == CurrentStateLocation.Column - 1) ||
                            (Destination.Row == CurrentStateLocation.Row + 1 && Destination.Column == CurrentStateLocation.Column + 1) ||
                            (Destination.Row == CurrentStateLocation.Row + 1 && Destination.Column == CurrentStateLocation.Column - 1) ||
                                  Destination.Row > CurrentStateLocation.Row + 1 ||
                                Destination.Column > CurrentStateLocation.Column + 1
                            )
                return true;

            return false;
        }
        public bool IsItEndState(GridLocation Destination)
        {
            if ((Destination.Row == Settings.EndLocation.Row) && (Destination.Column == Settings.EndLocation.Column))
                return true;

            return false;
        }
        public bool IsItBeginState(GridLocation Destination)
        {
            if ((Destination.Row == Settings.StartLocation.Row) && (Destination.Column == Settings.StartLocation.Column))
                return true;

            return false;
        }
        public List<GridLocation> AvailableNextMoves()
        {
            List<GridLocation> temp = new List<GridLocation> {
                new  GridLocation{ Row=CurrentStateLocation.Row +1 ,Column=CurrentStateLocation.Column},
                new  GridLocation{ Row=CurrentStateLocation.Row  ,Column=CurrentStateLocation.Column + 1},
                new  GridLocation{ Row=CurrentStateLocation.Row -1 ,Column=CurrentStateLocation.Column},
                new  GridLocation{ Row=CurrentStateLocation.Row  ,Column=CurrentStateLocation.Column -1},
            };
            List<GridLocation> moves = new List<GridLocation>();

            foreach (GridLocation x in temp)
            {
                var res = CheckValidateOfTheDestination(x);
                if ( !ResultHanderType.FailResults.Any(y => y == res ))
                {
                    moves.Add(x);
                    //Console.WriteLine(x.Row.ToString() + "," + x.Column.ToString());

                }
            }
            return moves;
        }

        //public List<MazeState> AvailableNextStates()
        //{
        //    List<GridLocation> temp = new List<GridLocation> {
        //        new  GridLocation{ Row=CurrentStateLocation.Row +1 ,Column=CurrentStateLocation.Column},
        //        new  GridLocation{ Row=CurrentStateLocation.Row  ,Column=CurrentStateLocation.Column + 1},
        //        new  GridLocation{ Row=CurrentStateLocation.Row -1 ,Column=CurrentStateLocation.Column},
        //        new  GridLocation{ Row=CurrentStateLocation.Row  ,Column=CurrentStateLocation.Column -1},
        //    };
        //    List<MazeState> next_states = new List<MazeState>();

        //    foreach (GridLocation x in temp)
        //    {
        //        var res = CheckValidateOfTheDestination(x);
        //        if (!ResultHanderType.FailResults.Any(y => y == res))
        //        {
        //            var NextStatePath = new List<GridLocation>(Settings.CurrentPath);
        //            NextStatePath.Add(x);

        //            var playerLocation = new GridLocation { Row = x.Row, Column = x.Column };

        //            var next_cost = Settings.CurrentCost + GridArr[x.Row - 1, x.Column - 1].Cost;

        //            next_states.Add(new MazeState(Settings,next_cost, playerLocation, NextStatePath));
        //        }
        //    }
        //    return next_states;
        //}


        public bool CanMoveUp()
        {
            var res =CheckValidateOfTheDestination( new GridLocation { Row = CurrentStateLocation.Row - 1, Column = CurrentStateLocation.Column  });

            if ( ResultHanderType.FailResults.Any(x => x == res && x!=ResultType.SameAsLastMove ))
                return false;

            return true;
        }
        public bool CanMoveDown()
        {
            var res = CheckValidateOfTheDestination(new GridLocation { Row = CurrentStateLocation.Row + 1, Column = CurrentStateLocation.Column });
        
            if ( ResultHanderType.FailResults.Any(x => x == res && x != ResultType.SameAsLastMove))
                return false;
            //if (res == ResultType.CanNotMoveBecauseItIllegalMove || res == ResultType.CanNotMoveBecauseOfWater || res == ResultType.OutOfGrid)
            //    return false;

            return true;
        }
        public bool CanMoveRight()
        {
            var res = CheckValidateOfTheDestination(new GridLocation { Row = CurrentStateLocation.Row , Column = CurrentStateLocation.Column +1});

            if ( ResultHanderType.FailResults.Any(x => x == res && x != ResultType.SameAsLastMove))
                return false;
            //if (res == ResultType.CanNotMoveBecauseItIllegalMove || res == ResultType.CanNotMoveBecauseOfWater || res == ResultType.OutOfGrid)
            //    return false;

            return true;
        }
        public bool CanMoveLeft()
        {
            var res = CheckValidateOfTheDestination(new GridLocation { Row = CurrentStateLocation.Row , Column = CurrentStateLocation.Column -1});

            if ( ResultHanderType.FailResults.Any(x => x == res && x != ResultType.SameAsLastMove))
                return false;
            //if (res == ResultType.CanNotMoveBecauseItIllegalMove || res == ResultType.CanNotMoveBecauseOfWater || res == ResultType.OutOfGrid)
            //    return false;

            return true;
        }
        public ResultType MoveRight()
        {
            return Move(new GridLocation { Row = CurrentStateLocation.Row, Column = CurrentStateLocation.Column + 1 });
        }
        public ResultType MoveLeft()
        {
            return Move(new GridLocation { Row = CurrentStateLocation.Row, Column = CurrentStateLocation.Column - 1 });
        }
        public ResultType MoveUp()
        {
            return Move(new GridLocation { Row = CurrentStateLocation.Row -1 , Column = CurrentStateLocation.Column });
        }
        public ResultType MoveDown()
        {
            return Move(new GridLocation { Row = CurrentStateLocation.Row + 1, Column = CurrentStateLocation.Column  });
        }

        //public List<MazeState> AvailableNextStates()
        //{
        //    if (CanMoveUp())
        //    {
        //        return new List<MazeState> { };
        //    }
        //    if (CanMoveDown())
        //    {
        //        return new List<MazeState> { };
        //    }
        //    if (CanMoveLeft())
        //    {
        //        return new List<MazeState> { };
        //    }
        //    if (CanMoveRight())
        //    {
        //        return new List<MazeState> { };
        //    }

        //    return new List<MazeState>() { };
        //}
    }
}
