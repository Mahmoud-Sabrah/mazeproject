﻿using MazeProject.Enums;
using MazeProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MazeProject.Models
{
   public class MazeWatcher :IMazeSharedMethods
    {

        public int GridRows { get; private set; }
        public int GridColumns { get; private set; }
        public GridLocation StartLocation { get; private set; }
        public GridLocation EndLocation { get; private set; }
        public List<GridLocation> CurrentPath { get; private set; }
        public GridLocation CurrentPlayerLocation { get;  set; }
        public List<MazeState> States { get; set; }
        public int CurrentCost { get; set; } = 0;
        public int StatesCount { get; set; } = 0;
        public GridCell[,] _maze { get; private set; }

        public MazeWatcher(int _rows, int _columns,GridLocation _startlocation,GridLocation _endlocation)
        {
            // if the user use wrong initalize values than use default values for all properties
            if (_rows <= 0 || _columns <= 0 ||
              _startlocation.Row > _rows || _startlocation.Row <= 0 || _startlocation.Column > _columns || _startlocation.Column <= 0 ||
              _endlocation.Row > _rows || _endlocation.Row <= 0 || _endlocation.Column > _columns || _endlocation.Column <= 0 )

            {
                _rows = 4;
                _columns = 4;
                _startlocation = new GridLocation{Row=1,Column=1 };
                _endlocation = new GridLocation { Row = 4, Column = 4 };
            }

            this.GridRows = _rows;
            this.GridColumns = _columns;
            this.StartLocation = _startlocation ;
            this.EndLocation = _endlocation;
            this.CurrentPlayerLocation = new GridLocation() { Row = StartLocation.Row, Column = StartLocation.Column };
            this.CurrentPath = new List<GridLocation> { new GridLocation { Row = StartLocation.Row, Column = StartLocation.Column } };
            this.States = new List<MazeState>();

            GenerateRandomMaze();
            this.States.Add(new MazeState(this));
        }

        public MazeWatcher(int _rows, int _columns, GridLocation _startlocation, GridLocation _endlocation,GridCell[,] _maze)
        {
            // if the user use wrong initalize values than use default values for all properties
            if (_rows <= 0 || _columns <= 0 ||
              _startlocation.Row > _rows || _startlocation.Row <= 0 || _startlocation.Column > _columns || _startlocation.Column <= 0 ||
              _endlocation.Row > _rows || _endlocation.Row <= 0 || _endlocation.Column > _columns || _endlocation.Column <= 0)

            {
                _rows = 4;
                _columns = 4;
                _startlocation = new GridLocation { Row = 1, Column = 1 };
                _endlocation = new GridLocation { Row = 4, Column = 4 };
            }

            this.GridRows = _rows;
            this.GridColumns = _columns;
            this.StartLocation = _startlocation;
            this.EndLocation = _endlocation;
            this.CurrentPlayerLocation = new GridLocation() { Row = StartLocation.Row, Column = StartLocation.Column };
            this.CurrentPath = new List<GridLocation> { new GridLocation { Row = StartLocation.Row, Column = StartLocation.Column } };
            this.States = new List<MazeState>();

            this._maze = _maze;
            this.States.Add(new MazeState(this));
        }

        public void ReloadWithTheSameMaze()
        {
            this.CurrentPlayerLocation = new GridLocation { Row = StartLocation.Row, Column = StartLocation.Column };
            this.CurrentPath = new List<GridLocation> { new GridLocation { Row = StartLocation.Row, Column = StartLocation.Column } };
            this.States = new List<MazeState>();
            this.CurrentCost = 0;
            this.StatesCount = 0;
            this.States.Add(new MazeState(this));
        }

        public void GenerateRandomMaze()
        {
           _maze = new GridCell[GridRows, GridColumns];
           var _types =  Enum.GetNames(typeof(GridCellType)).Length;
           var  rand = new Random();
            for (int i=0;i< _maze.GetLength(0);i++)
            {
                for(int j=0; j<_maze.GetLength(1);j++)
                {
                    if((i == StartLocation.Row -1  && j == StartLocation.Column -1 ) || (i == EndLocation.Row-1 && j == EndLocation.Column-1))
                    {
                        _maze[i, j] = new GridCell((GridCellType)rand.Next(0, _types-1));
                        continue;
                    }
                   
                    _maze[i, j] = new GridCell((GridCellType)rand.Next(0, _types ));
                }
            }
        }

     
        public ResultType Move(GridLocation Destination)
        {
            return States.Last().Move(Destination);
        }
        public ResultType CheckValidateOfTheDestination(GridLocation Destination)
        {
            return States.Last().CheckValidateOfTheDestination(Destination);
        }
        public bool IsItOutOfGrid(GridLocation Destination)
        {
            return States.Last().IsItOutOfGrid(Destination);
        }
        public bool IsItWater(GridLocation Destination)
        {
            return States.Last().IsItWater(Destination);
        }
        public bool IsItIllegalMove(GridLocation Destination)
        {
            return States.Last().IsItIllegalMove(Destination);
        }
        public bool IsItEndState(GridLocation Destination)
        {
            return States.Last().IsItEndState(Destination);
        }
        public bool IsItBeginState(GridLocation Destination)
        {
            return States.Last().IsItBeginState(Destination);
        }
        public List<GridLocation> GetLastStateAvailableMoves()
        {
            return new List<GridLocation>(States.Last().NextMoves);
        }
        public bool CanMoveUp()
        {
            return States.Last().CanMoveUp();
        }
        public bool CanMoveDown()
        {
            return States.Last().CanMoveDown();
        }
        public bool CanMoveRight()
        {
            return States.Last().CanMoveRight();
        }
        public bool CanMoveLeft()
        {
            return States.Last().CanMoveLeft();
        }

        public GridLocation GetLastStateUpLocation()
        {
            var x = States.Last().CurrentStateLocation.Row;
            var y = States.Last().CurrentStateLocation.Column;
            return new GridLocation { Row = States.Last().CurrentStateLocation.Row - 1, Column = States.Last().CurrentStateLocation.Column };
        }
        public GridLocation GetLastStateDownLocation()
        {
            return new GridLocation { Row = States.Last().CurrentStateLocation.Row + 1, Column = States.Last().CurrentStateLocation.Column };
        }
        public GridLocation GetLastStateRightLocation()
        {
            return new GridLocation { Row = States.Last().CurrentStateLocation.Row , Column = States.Last().CurrentStateLocation.Column +1 };
        }
        public GridLocation GetLastStateLeftLocation()
        {
            return new GridLocation { Row = States.Last().CurrentStateLocation.Row, Column = States.Last().CurrentStateLocation.Column -1};
        }
       


        public ResultType MoveRight()
        {
            return States.Last().MoveRight();
        }

        public ResultType MoveLeft()
        {
            return States.Last().MoveLeft();
        }

        public ResultType MoveUp()
        {
            return States.Last().MoveUp();
        }

        public ResultType MoveDown()
        {
            return States.Last().MoveDown();
        }

        public MazeState GetLastState()
        {
            return States.Last();
        }

        public List<GridLocation> GetLastStateNextMoves()
        {
            return States.Last().NextMoves;
        }

        public MazeState UndoLastState()
        {
            if (States.Count == 1)
                return States.Last();

            this.StatesCount--;
            var last = this.CurrentPath.Last();                             
            this.CurrentCost -= _maze[last.Row - 1, last.Column - 1].Cost;
            this.CurrentPath.Remove(last);
            this.CurrentPlayerLocation = new GridLocation { Row = this.CurrentPath.Last().Row, Column = this.CurrentPath.Last().Column };
            this.States.Remove(this.States.Last());
            return States.Last();
        }

        public  bool IsItVisited(GridLocation Destination)
        {
            if (States.SingleOrDefault(x => x.CurrentStateLocation.Row == Destination.Row && x.CurrentStateLocation.Column == Destination.Column) != null)
                return true;

            return false;
        }
        

}
}
