﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazeProject.Enums;

namespace MazeProject.Models
{
    public class GridCell
    {
        public int Cost { get; }
        public GridCellType CellType { get;}

        public GridCell(GridCellType _type)
        {
            this.CellType = _type;

            if(_type == GridCellType.Path)
                Cost = 10;
            else if (_type == GridCellType.Sand)
                Cost = 30;
            else if (_type == GridCellType.Mountain)
                Cost = 100;            
            else if (_type == GridCellType.Water)
                Cost = -1;
        }
    }
}
