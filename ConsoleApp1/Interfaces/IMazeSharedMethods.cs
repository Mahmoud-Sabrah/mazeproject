﻿using MazeProject.Enums;
using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Interfaces
{
    interface IMazeSharedMethods
    {
        ResultType Move(GridLocation Destination);
        ResultType CheckValidateOfTheDestination(GridLocation Destination);
        bool IsItOutOfGrid(GridLocation Destination);
        bool IsItWater(GridLocation Destination);
        bool IsItIllegalMove(GridLocation Destination);
        bool IsItEndState(GridLocation Destinaton);
        bool IsItBeginState(GridLocation Destinaton);
        bool CanMoveUp();
        bool CanMoveDown();
        bool CanMoveRight();
        bool CanMoveLeft();
        ResultType MoveRight();
        ResultType MoveLeft();
        ResultType MoveUp();
        ResultType MoveDown();
    }
}
