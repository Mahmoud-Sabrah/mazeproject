﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MazeProject.Algorithms
{
  public class DfsSolve
    {
        GridWatcher grd;
        Stack<GridNode> Dfstack;
        List<GridNode> Path;

        public DfsSolve(GridWatcher _grid)
        {
            this.grd = _grid;
            Path = new List<GridNode>();
            Dfstack = new Stack<GridNode>();
        }

        public bool Solve()
        {
            List<GridNode> Nodes = grd.Nodes;
            Dfstack.Push(Nodes.First());
            Path = new List<GridNode> { Nodes.First() };
            while (Dfstack.Count() !=0)
            {
                GridNode node = Dfstack.Pop();
                if(node.Visited != true )
                    node.Visited = true;                
                List<GridNode> childs = node.Neighbors;
                foreach (GridNode x in childs)
                {
                    if (x.Visited)
                        continue;
                    if(x.NodeLocation.Row == this.grd.EndNode.NodeLocation.Row && x.NodeLocation.Column == this.grd.EndNode.NodeLocation.Column)
                    {
                        Path.Add(x);
                        return true;
                    }
                    else
                        Dfstack.Push(x);

                }
            }
            return false;
        }    

    }
}
