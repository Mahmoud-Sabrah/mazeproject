﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MazeProject.Algorithms
{
    public class BfsSolve
    {
        GridWatcher grd;
        Queue<GridNode> BfsQueue;
        List<GridNode> Path;

        public BfsSolve(GridWatcher _grid)
        {
            this.grd = _grid;
            Path = new List<GridNode>();
            BfsQueue = new Queue<GridNode>();
        }

        public bool Solve()
        {
            List<GridNode> Nodes = grd.Nodes;
            Path = new List<GridNode> { Nodes.First() };


            BfsQueue.Enqueue(Nodes.First());


            while (BfsQueue.Count > 0)
            {
                var element = BfsQueue.Dequeue();
                if (element.Neighbors.Count > 0)
                {
                    foreach (var item in element.Neighbors)
                    {
                        BfsQueue.Enqueue(item);
                        if (item.NodeLocation.Row == this.grd.EndNode.NodeLocation.Row && item.NodeLocation.Column == this.grd.EndNode.NodeLocation.Column)
                        {
                            return true;
                        }
                    }
                }
            }


            return false;
        }




    }
}
