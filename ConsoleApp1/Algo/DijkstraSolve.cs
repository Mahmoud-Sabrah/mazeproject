﻿using MazeProject.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ConsoleApp1.Algo
{
    public class DijkstraSolve
    {
        GridWatcher grd;

        public DijkstraSolve(GridWatcher _grid)
        {
            this.grd = _grid;

            foreach (var x in _grid.Nodes)
            {
                x.Visited = false;
                x.MinCostToStart = 0;
                x.NearNodeFromStart = null;
            }
            TrySolve();
        }
        public List<GridNode> TrySolve()
        {
            Solve();
            var shortestPath = new List<GridNode>();
            shortestPath.Add(grd.EndNode);
            BuildShortestPath(shortestPath, grd.EndNode);
            shortestPath.Reverse();
            return shortestPath;
        }
        private void BuildShortestPath(List<GridNode> list, GridNode node)
        {
            if (node.NearNodeFromStart == null)
                return;
            list.Add(node.NearNodeFromStart);
            BuildShortestPath(list, node.NearNodeFromStart);
        }
        public bool Solve()
        {
            GridNode StartNode = grd.StartNode;
            StartNode.MinCostToStart = 0;

            var prioQueue = new List<GridNode>();
            prioQueue.Add(StartNode);
            do
            {
                prioQueue = prioQueue.OrderBy(x => x.MinCostToStart).ToList();
                var node = prioQueue.First();
                prioQueue.Remove(node);
                foreach (var childNode in node.Neighbors.OrderBy(x => x.Cost))
                {
                    if (childNode.Visited)
                        continue;
                    if (childNode.MinCostToStart == 0 ||
                        node.MinCostToStart + node.Cost < childNode.MinCostToStart)
                    {
                        childNode.MinCostToStart = node.MinCostToStart + node.Cost;
                        childNode.NearNodeFromStart = node;
                        if (!prioQueue.Contains(childNode))
                            prioQueue.Add(childNode);
                    }
                }
                node.Visited = true;
                if (node.NodeLocation.Row == grd.EndNode.NodeLocation.Row && node.NodeLocation.Column == grd.EndNode.NodeLocation.Column)
                    return true;
            } while (prioQueue.Any());

            return false;
        }
    }
}
