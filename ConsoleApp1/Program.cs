﻿using System;
using MazeProject.Models;
using MazeProject.Enums;
using MazeProject.Algorithms;
using ConsoleApp1.Algo;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
       static GridWatcher grd;
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("...........................................................");
                grd = new GridWatcher(3, 3, new GridLocation() { Row = 1, Column = 1 }, new GridLocation() { Row = 3, Column = 3 });
                for (int i = 0; i < grd._maze.GetLength(0); i++)
                {
                    for (int j = 0; j < grd._maze.GetLength(1); j++)
                    {
                        Console.Write(grd._maze[i, j].CellType.ToString() + " ");
                    }
                    Console.WriteLine();
                }
                
                //Console.WriteLine(DFS().ToString());
                //Console.WriteLine(BFS().ToString());
                Dijkstra();

            }

        }


        public static void Human()
        {
            while (true)
            {
                Console.Write("Enter the direction:");

                ConsoleKeyInfo key = Console.ReadKey();
                if (key.Key == ConsoleKey.UpArrow)
                {
                    Console.WriteLine(grd.Move(Enums.Direction.Up).ToString());
                }
                else if (key.Key == ConsoleKey.DownArrow)
                {
                    Console.WriteLine(grd.Move(Enums.Direction.Down).ToString());
                }
                else if (key.Key == ConsoleKey.RightArrow)
                {
                    Console.WriteLine(grd.Move(Enums.Direction.Right).ToString());
                }
                else if (key.Key == ConsoleKey.LeftArrow)
                {
                    Console.WriteLine(grd.Move(Enums.Direction.Left).ToString());
                }

            }
        }

        public static bool DFS()
        {
            Console.WriteLine("press any key to use dfs");
            Console.ReadLine();
            DfsSolve dfs = new DfsSolve(grd);
           return dfs.Solve();
        }

        public static bool BFS()
        {
            Console.WriteLine("press any key to use bfs");
            Console.ReadLine();
            BfsSolve bfs = new BfsSolve(grd);
            return bfs.Solve();
        }

        public static void Dijkstra()
        {

            Console.WriteLine("press any key to use Dijkstra");
            Console.ReadLine();
            DijkstraSolve dij = new DijkstraSolve(grd);


            var list = dij.TrySolve();
            var i = -grd.Nodes.First().Cost;
            foreach (var node in list)
            {
                Console.WriteLine($"{node.NodeLocation.Row.ToString()},{node.NodeLocation.Column.ToString()}");
                
                i += node.Cost;
            }

            Console.WriteLine("TotalCost=" + i.ToString());
        }
    }
}
