﻿using ConsoleApp1.Enums;
using MazeProject.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Models
{
   public class GridWatcher
    {
        public int GridRows { get; private set; }
        public int GridColumns { get; private set; }
        public GridCell[,] _maze { get; private set; }
        public GridNode StartNode { get; private set; }
        public GridNode EndNode { get; private set; }
        public GridNode PlayerNode { get; set; }

        public List<GridNode> CurrentPath { get; private set; }
        public List<GridNode> Nodes { get; set; }
        public int TotalCost { get; set; } = 0;



        public GridWatcher(int _rows, int _columns, GridLocation _startlocation, GridLocation _endlocation)
        {        
            // if the user use wrong initalize values than use default values for all properties
            if (_rows <= 0 || _columns <= 0 ||
              _startlocation.Row > _rows || _startlocation.Row <= 0 || _startlocation.Column > _columns || _startlocation.Column <= 0 ||
              _endlocation.Row > _rows || _endlocation.Row <= 0 || _endlocation.Column > _columns || _endlocation.Column <= 0)

            {
                _rows = 4;
                _columns = 4;
                _startlocation = new GridLocation { Row = 1, Column = 1 };
                _endlocation = new GridLocation { Row = 4, Column = 4 };
            }

            this.GridRows = _rows;
            this.GridColumns = _columns;
            this.CurrentPath = new List<GridNode>();
            this.Nodes = new List<GridNode>();

            GenerateRandomMaze(_startlocation,_endlocation);
            FillGraph(_startlocation,_endlocation);
        }
        public GridWatcher(int _rows, int _columns, GridLocation _startlocation, GridLocation _endlocation, GridCell[,] _maze)
        {
            // if the user use wrong initalize values than use default values for all properties
            if (_rows <= 0 || _columns <= 0 ||
              _startlocation.Row > _rows || _startlocation.Row <= 0 || _startlocation.Column > _columns || _startlocation.Column <= 0 ||
              _endlocation.Row > _rows || _endlocation.Row <= 0 || _endlocation.Column > _columns || _endlocation.Column <= 0)

            {
                _rows = 4;
                _columns = 4;
                _startlocation = new GridLocation { Row = 1, Column = 1 };
                _endlocation = new GridLocation { Row = 4, Column = 4 };
            }

            this.GridRows = _rows;
            this.GridColumns = _columns;
            this.CurrentPath = new List<GridNode>();
            this.Nodes = new List<GridNode>();

            this._maze = _maze;
            FillGraph(_startlocation, _endlocation);
        }
        public void GenerateRandomMaze(GridLocation _startlocation, GridLocation _endlocation)
        {
            _maze = new GridCell[GridRows, GridColumns];
            var _types = Enum.GetNames(typeof(GridCellType)).Length;
            var rand = new Random();
            for (int i = 0; i < _maze.GetLength(0); i++)
            {
                for (int j = 0; j < _maze.GetLength(1); j++)
                {
                    if ((i == _startlocation.Row - 1 && j ==_startlocation.Column - 1) || (i == _endlocation.Row - 1 && j == _endlocation.Column - 1))
                    {
                        _maze[i, j] = new GridCell((GridCellType)rand.Next(0, _types - 1));
                        continue;
                    }

                    _maze[i, j] = new GridCell((GridCellType)rand.Next(0, _types));
                }
            }
        }
        public void FillGraph(GridLocation _startlocation, GridLocation _endlocation)
        {
            Nodes = new List<GridNode>();
            for(int i=0; i<_maze.GetLength(0);i++)
            {
                for(int j=0;j<_maze.GetLength(1) ;j++)
                {
                    GridNode temp = new GridNode(this, new GridLocation { Row = i + 1, Column = j + 1 }, _maze[i, j].Cost);
                    Nodes.Add(temp);
                    if (temp.NodeLocation.Row == _startlocation.Row && temp.NodeLocation.Column == _startlocation.Column)
                    {
                        StartNode = temp;
                        PlayerNode = temp;
                        CurrentPath.Add(StartNode);
                    }
                    if (temp.NodeLocation.Row == _endlocation.Row && temp.NodeLocation.Column == _endlocation.Column)
                        EndNode = temp;
                }
            }

            foreach (var item in Nodes)
            {
                item.FillNeighborsNodes();
            }
        }


        public ResultType CheckValidateOfTheDestination(GridLocation NodeLocation, GridLocation Destination)
        {
            if (IsItOutOfGrid(Destination))
                return ResultType.OutOfGrid;
            else if (IsItIllegalMove(NodeLocation, Destination))
                return ResultType.CanNotMoveBecauseItIllegalMove;
            else if (IsItWater(Destination))
                return ResultType.CanNotMoveBecauseOfWater;
            else if (IsItBeginLocation(Destination))
                return ResultType.BeginState;
            else if (IsItEndLocation(Destination))
                return ResultType.EndState;            
            return ResultType.Done;
        }
        public bool IsItOutOfGrid(GridLocation Destination)
        {
            if (Destination.Row > this.GridRows ||
                Destination.Column > this.GridColumns ||
                Destination.Row <= 0 || // <= 0 because the user must start from (1) as index not zero 
                Destination.Column <= 0
                )
                return true;

            return false;

        }
        public bool IsItWater(GridLocation Destination)
        {
            if (_maze[Destination.Row - 1, Destination.Column - 1].CellType == GridCellType.Water)
                return true;
            return false;
        }
        public bool IsItEndLocation(GridLocation Destination)
        {
            if ((Destination.Row == this.EndNode.NodeLocation.Row) && (Destination.Column == this.EndNode.NodeLocation.Column))
                return true;

            return false;
        }
        public bool IsItBeginLocation(GridLocation Destination)
        {
            if ((Destination.Row == this.StartNode.NodeLocation.Row) && (Destination.Column == this.StartNode.NodeLocation.Column))
                return true;

            return false;
        }
        public bool IsItIllegalMove(GridLocation NodeLocation, GridLocation Destination)
        {
            if (Destination.Row < NodeLocation.Row - 1 ||
                            Destination.Column < NodeLocation.Column - 1 ||
                            (Destination.Row == NodeLocation.Row - 1 && Destination.Column == NodeLocation.Column + 1) ||
                            (Destination.Row == NodeLocation.Row - 1 && Destination.Column == NodeLocation.Column - 1) ||
                            (Destination.Row == NodeLocation.Row + 1 && Destination.Column == NodeLocation.Column + 1) ||
                            (Destination.Row == NodeLocation.Row + 1 && Destination.Column == NodeLocation.Column - 1) ||
                                  Destination.Row > NodeLocation.Row + 1 ||
                                Destination.Column > NodeLocation.Column + 1
                            )
                return true;

            return false;
        }


        public List<GridLocation> AvailableNextMoves(GridLocation NodeLocation)
        {
            List<GridLocation> temp = new List<GridLocation> {
                new  GridLocation{ Row=NodeLocation.Row +1 ,Column=NodeLocation.Column},
                new  GridLocation{ Row=NodeLocation.Row  ,Column=NodeLocation.Column + 1},
                new  GridLocation{ Row=NodeLocation.Row -1 ,Column=NodeLocation.Column},
                new  GridLocation{ Row=NodeLocation.Row  ,Column=NodeLocation.Column -1},
            };
            List<GridLocation> moves = new List<GridLocation>();

            foreach (GridLocation x in temp)
            {
                var res = CheckValidateOfTheDestination(NodeLocation, x);
                if (!ResultHanderType.FailResults.Any(y => y == res))
                {
                    moves.Add(x);
                    //Console.WriteLine(x.Row.ToString() + "," + x.Column.ToString());

                }
            }
            return moves;
        }


        public ResultType Move(Direction To)
        {
            GridLocation Destination = new GridLocation() ;


            if(To == Direction.Up)
            {
                Destination = new GridLocation() { Row = PlayerNode.NodeLocation.Row - 1, Column =PlayerNode.NodeLocation.Column };
            }
            else if(To == Direction.Right)
            {
                Destination = new GridLocation() { Row = PlayerNode.NodeLocation.Row , Column = PlayerNode.NodeLocation.Column +1};
            }
            else if (To == Direction.Down)
            {
                Destination = new GridLocation() { Row = PlayerNode.NodeLocation.Row +1, Column = PlayerNode.NodeLocation.Column };
            }
            else if (To == Direction.Left)
            {
                Destination = new GridLocation() { Row = PlayerNode.NodeLocation.Row , Column = PlayerNode.NodeLocation.Column -1};
            }



            var _res = CheckValidateOfTheDestination(PlayerNode.NodeLocation, Destination);


            if (ResultHanderType.FailResults.Any(x => x == _res))
                return _res;

            //change the player node to the new location
            PlayerNode = Nodes.SingleOrDefault(x => x.NodeLocation.Row == Destination.Row && x.NodeLocation.Column == Destination.Column);
            CurrentPath.Add(PlayerNode); //store the destination state location int the current state 
            TotalCost += _maze[Destination.Row - 1, Destination.Column - 1].Cost;
            return _res;
        }



    }
}
