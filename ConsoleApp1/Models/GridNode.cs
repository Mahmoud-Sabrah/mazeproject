﻿using MazeProject.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeProject.Models
{
  public class GridNode
    {
        public GridWatcher Parent { get; set; }
        public GridLocation NodeLocation { get; set; }
        public List<GridNode> Neighbors { get; set; }
        public int Cost { get; set; }

        public bool Visited { get; set; } = false;
        public int MinCostToStart { get; set; } = 0;
        public GridNode NearNodeFromStart { get; set; } 


        public GridNode(GridWatcher Parent, GridLocation CurrentStateLocation, int Cost)
        {
            this.NodeLocation = CurrentStateLocation;
            this.Cost = Cost;
            this.Parent = Parent;
        }

        public void FillNeighborsNodes()
        {
            Neighbors = new List<GridNode>();
            List<GridLocation> LocationNeighbors = Parent.AvailableNextMoves(NodeLocation);
            
            foreach(var item in Parent.Nodes)
            {
                if(LocationNeighbors.Where(x=>x.Row == item.NodeLocation.Row && x.Column == item.NodeLocation.Column).Count() !=0)
                {
                    Neighbors.Add(item);
                }
            }
        }

    }
}
